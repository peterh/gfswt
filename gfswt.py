#!/usr/bin/env python
# -*- coding: utf-8 -*-

HEADER = '''
**Header:**
<br/>
* title               :gfswt.py
* description         :detection of weather-types in forecasts.
* author              :Peter Hoffmann
* email               :peterh@pik-potsdam.de
* affiliation         :potsdam institute for climate impacts research (PIK)
* research department :climate resilience
* working group       :hydro-climatic risks
* project             :captainrain
* date                :2024-01-25
* version             :1.0
* usage               :python3 gfswt.py
* python_version      :3.10.0  
***
**Requirements**
<br/>
* python-dateutil==2.8.2
* siphon==0.9
* matplotlib==3.8.2
* numpy==1.23.1
* netCDF4==1.6.5
* Cartopy==0.22.0
* scipy==1.8.0
* xarray==2023.10.1
* rich==13.7.0
***
'''

import os
import sys
from pathlib import Path
from datetime import datetime, timedelta,date
from siphon.catalog import TDSCatalog
from siphon.ncss import NCSS
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as P
import numpy as N
from netCDF4 import Dataset, num2date,date2num
from matplotlib.offsetbox import AnchoredText
import cartopy.crs as ccrs
from scipy.interpolate import griddata
import matplotlib.image as image
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
import joblib

from xarray.backends import NetCDF4DataStore
import xarray as xr
import warnings
warnings.filterwarnings('ignore')
from rich.console import Console
from rich.markdown import Markdown
from rich.table import Table

console = Console(record=True)

P.style.use('classic')
P.style.use('bmh')   

params = {'legend.fontsize': 10,
          'grid.color':'black', 
          'axes.grid':True,
          'grid.linestyle':'dotted',
          'xtick.direction':'out',
          'ytick.direction':'out',
          'xtick.labelsize':14,
          'ytick.labelsize':14,
          'font.family': 'serif'}

P.rcParams.update(params)

setting = ['JO','OBJ','read']
#setting = ['EU','OBJ','read'] 
#setting = ['EU','GWL','read']

EU_GWL = {'W':'windy','BM':'bright','TRM':'rainy','TRW':'thunderstorm','HM':'sunny','SW':'hot','NW':'icy','HB':'icy','HF':'sunny','WS':'bright','TB':'stormy','HN':'sunny','WW':'windy','TM':'flood','SE':'hot','N':'icy','HNF':'sunny','S':'hot','NE':'flood'}
JO_OBJ = {'WT01':'bright','WT02':'bright','WT03':'sunny','WT04':'bright','WT05':'sunny','WT06':'rainy','WT07':'sunny','WT08':'rainy','WT09':'bright','WT10':'bright','WT11':'bright','WT12':'sunny','WT13':'rainy','WT14':'bright','WT15':'stormy','WT16':'bright','WT17':'flood','WT18':'flood','WT19':'bright','WT20':'sunny'}
EU_OBJ = {'WT01':'windy','WT02':'windy','WT03':'bright','WT04':'hot','WT05':'icy','WT06':'icy','WT07':'sunny','WT08':'rainy','WT09':'stormy','WT10':'thunderstorm','WT11':'rainy','WT12':'icy','WT13':'icy','WT14':'stormy','WT15':'sunny','WT16':'bright','WT17':'rainy','WT18':'flood','WT19':'thunderstorm','WT20':'rainy'}

if((setting[0]=='EU')&(setting[1]=='GWL')): gs = EU_GWL
if((setting[0]=='JO')&(setting[1]=='OBJ')): gs = JO_OBJ
if((setting[0]=='EU')&(setting[1]=='OBJ')): gs = EU_OBJ

def mf(List):
    return max(set(List), key = List.count)

if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    bundle_dir = Path(sys._MEIPASS)
else:
    bundle_dir = Path(__file__).parent

#bundle_dir = Path(getattr(sys, '_MEIPASS', Path.cwd()))
#bundle_dir = getattr(sys, '_MEIPASS', os.path.relpath(os.path.dirname(__file__)))
#bundle_dir = getattr(sys, '_MEIPASS', os.path.abspath(os.path.dirname(__file__)))
#bundle_dir = '.\%s\'%setting[1]
#bundle_dir = './%s/'%setting[1]

print (bundle_dir)

try:
    f = open("gfswt.joblib")
except FileNotFoundError:
    job = 'save'
else:
    job = 'read'

text = '''
# GFSWT-1.0
'''

md = Markdown(text)
console.print(md)

md = Markdown(HEADER)
console.print(md)

text = '''
**Setting:**
<br/>
* European Weather-Types
***
'''

md = Markdown(text)
console.print(md)

text = '''
**Reading:**
<br/>
* ERA5 Reanalysis Data of daily Z500 over Europe
* Time series of Weather-Types
***
'''

md = Markdown(text)
console.print(md)

if((setting[0]=='EU')&(setting[1]=='GWL')): nc = Dataset('%s/era5_z500_daily_1981-2022_2x2_EU.netcdf'%bundle_dir,'r')
if((setting[0]=='EU')&(setting[1]=='OBJ')): nc = Dataset('%s/era5_z500_daily_1951-1980_2x2_EU.netcdf'%bundle_dir,'r')
if((setting[0]=='JO')&(setting[1]=='OBJ')): nc = Dataset('%s/era5_z500_daily_1951-1980_2x2_JO.netcdf'%bundle_dir,'r')

lons = N.array(nc.variables['lon'][:]);nx = len(lons)
lats = N.array(nc.variables['lat'][:]);ny = len(lats)
dat = N.array(nc.variables['z500'][:])

tim = nc.variables['time']

tim = num2date(tim[:], units=tim.units,calendar=tim.calendar) 

tt = []

for it in tim:

    tt.append('%04i-%02i-%02i'%(it.year,it.month,it.day))

tt = N.array(tt)

nc.close()

nd = dat.shape[0]

if((setting[0]=='EU')&(setting[1]=='GWL')): fileC = '%s/GWL_1981-2022.dat'%bundle_dir
if((setting[0]=='EU')&(setting[1]=='OBJ')): fileC = '%s/era5_z500_daily_1951-1980_1x1_EU_20.lst'%bundle_dir
if((setting[0]=='JO')&(setting[1]=='OBJ')): fileC = '%s/era5_z500_daily_1951-1980_1x1_JO_20.lst'%bundle_dir

csv = N.genfromtxt(fileC,names=True,delimiter=';',dtype=None)

gw = N.array(csv['gw'],str)

gg = gw

gw[gw=='NWZ'] = 'NW'
gw[gw=='NWA'] = 'NW'
gw[gw=='SWZ'] = 'SW'
gw[gw=='SWA'] = 'SW'
gw[gw=='NEZ'] = 'NE'
gw[gw=='NEA'] = 'NE'
gw[gw=='SEZ'] = 'SE'
gw[gw=='SEA'] = 'SE'

gw[gw=='NFZ'] = 'NF'
gw[gw=='NFA'] = 'NF'
gw[gw=='HNFZ'] = 'HNF'
gw[gw=='HNFA'] = 'HNF'
gw[gw=='HFZ'] = 'HF'
gw[gw=='HFA'] = 'HF'
gw[gw=='HNZ'] = 'HN'
gw[gw=='HNA'] = 'HN'

gw[gw=='SZ'] = 'S'
gw[gw=='SA'] = 'S'
gw[gw=='NZ'] = 'N'
gw[gw=='NA'] = 'N'
gw[gw=='WZ'] = 'W'
gw[gw=='WA'] = 'W'
gw[gw=='U'] = 'W'
gw[gw=='WW'] = 'W' 
gw[gw=='WS'] = 'W'

gg = gw

phi = N.zeros((nd,ny,nx),float)

for d in range(nd):

    tmp = dat[d,:,:]
    phi[d,:,:] = (tmp-N.min(tmp))/(N.max(tmp)-N.min(tmp))

phi = N.reshape(phi,(nd,ny*nx))

X_train = phi[:,:]
y_train = gg[:]

text = '''
**Training:**
<br/>
* Decision Tree between Z500 and Weather-Types
***
'''

md = Markdown(text)
console.print(md)

if(job=='save'):

    #clf = DecisionTreeClassifier(criterion="entropy",random_state=0)
    #clf = DecisionTreeClassifier(criterion="gini",random_state=0,max_depth=6)
    clf = RandomForestClassifier(n_estimators=100)
    clf = clf.fit(X_train,y_train)

    joblib.dump(clf, "./gfswt.joblib")
    
    md = Markdown('save classifier: gfswt.joblib')
    console.print(md)
    
if(job=='read'):    
    
    clf = joblib.load("./gfswt.joblib")

    md = Markdown('read classifier: gfswt.joblib')
    console.print(md)

f = open('gfswt.csv','w')

f.write('dateA;dateB;cluster\n')

nt = 15#10#15
nh = 8#4#8


#for t in [10,9,8,7,6,5,4,3,2,1,0]:#[16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]:
for t in [15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]:    
#for t in [12,11,10,9,8,7,6,5,4,3]:

    now = datetime.utcnow() - timedelta(days=t)
    
    now = now.strftime("%Y%m%d")

    #if(t>2): gfs_url = 'https://thredds.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_onedeg/GFS_Global_onedeg_%s_0000.grib2/catalog.xml'%now
    #else: gfs_url = 'https://tds.scigw.unidata.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_onedeg/GFS_Global_onedeg_%s_0000.grib2/catalog.xml'%now

    #gfs_url = 'https://thredds.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_onedeg/GFS_Global_onedeg_%s_0000.grib2/catalog.xml'%now
    #gfs_url = 'https://thredds.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_onedeg/GFS_Global_onedeg_%s_0600.grib2/catalog.xml'%now
    #gfs_url = 'https://tds.scigw.unidata.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_onedeg/GFS_Global_onedeg_%s_0000.grib2/catalog.xml'%now
    #https://thredds.rda.ucar.edu/thredds/catalog/files/g/ds084.1/2024/20240101/catalog.html
    #https://www.nco.ncep.noaa.gov/pmb/products/gfs/
    
    #gfs_url = 'https://thredds.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_onedegree_noaaport/GFS_Global_onedeg_noaaport_%s_0000.grib2/catalog.xml'%now
      
    gfs_url = 'https://thredds.ucar.edu/thredds/catalog/grib/NCEP/GFS/Global_onedeg/GFS_Global_onedeg_%s_0000.grib2/catalog.xml'%(now)
    
    try:
  
       cat = TDSCatalog(gfs_url)
       ncss = cat.datasets['GFS_Global_onedeg_%s_0000.grib2'%(now)].subset()
       #ncss = cat.datasets['GFS_Global_onedeg_noaaport_%s_0000.grib2'%now].subset()

       print (t,now)
    
       #nt = 15#10#15
       #nh = 8#4#8
 
       gfs = N.zeros((nt,ny,nx),float)
       ftime = []

       query = ncss.query()

       query.var = set()
       query.variables('Geopotential_height_isobaric')
       query.vertical_level(50000)

       now = datetime.utcnow() - timedelta(days=t)
       query.time_range(now, now + timedelta(days=nt))

       query.lonlat_box(west=N.min(lons), east=N.max(lons), north=N.max(lats), south=N.min(lats))
       data = ncss.get_data(query)

       keys = list(data.variables.keys())

       for key in keys:

           if(key[0:4]=='time'): 
        
               tkey = key

       ds = xr.open_dataset(NetCDF4DataStore(data))     

       var = ds['Geopotential_height_isobaric']       
       lat = var['latitude']
       lon = var['longitude']
        
       gph = var.squeeze()

       lat = N.array(lat)
       lon = N.array(lon)
       gph = N.array(gph,float)
    
       #print (gph.shape)
    
       gph = gph[:nt*nh,:,:]
       gph = N.reshape(gph,(nt,nh,len(lat),len(lon))) 
       gph = N.mean(gph,1)   

       x,y = N.meshgrid(lon,lat)
       x = N.array(N.ravel(x))
       y = N.array(N.ravel(y))
    
       if(setting[0]=='EU'): x2 = lons+360.
       if(setting[0]=='JO'): x2 = lons
    
       y2 = lats

       x2, y2 = N.meshgrid(x2,y2)

       gfs = N.zeros((nt,ny,nx),float)

       ftime = []

       for t in range(nt):
    
           fcst = now + timedelta(days=t)

           ftime.append(str(fcst)[0:10])

           z = N.array(N.ravel(gph[t,:,:]))

           tmp = griddata((x,y),z,(x2,y2), method='linear')
        
           tmp = (tmp-N.min(tmp))/(N.max(tmp)-N.min(tmp))

           gfs[t,:,:] = tmp

       now = str(now)[0:10]

       X_test = N.reshape(gfs,(nt,ny*nx))

       y_pred = clf.predict(X_test)

       table = Table(title='GFS_Global_onedeg_%s_0000'%(now))
       #table = Table(title='GFS_Global_onedeg_noaaport_%s_0000'%now)

       table.add_column("Initial Date", justify="left", style="cyan", no_wrap=True)
       table.add_column("Forecast Date", justify="left",style="magenta")
       table.add_column("GWL", justify="lefz", style="green")

       for t in range(nt):

           f.write('%s;%s;%s\n'%(now,ftime[t],y_pred[t]))

           table.add_row("%s"%now, "%s"%ftime[t], "%s"%y_pred[t])
        
       console = Console()
       console.print(table)
    
    except:

       #now = str(now)[0:10]

       for d in range(nt):
       
           now = datetime.utcnow() - timedelta(days=t)
           now = str(now)[0:10]

           f.write('%s;%s;%s\n'%(now,now,'NaN'))
           
       print(t,now,'missing')

f.close()

# ##

text = '''
**Plotting:**
<br/>
* GFS Z500 Forecasted Fields
***
'''

md = Markdown(text)
console.print(md)

fig = P.figure(figsize=(20,6))

for t in range(nt):

    ax = fig.add_subplot(3,5,t+1, projection=ccrs.PlateCarree())
    #ax = fig.add_subplot(2,5,t+1, projection=ccrs.PlateCarree())
    ax.set_extent([N.min(lons),N.max(lons), N.min(lats), N.max(lats)], ccrs.PlateCarree())
    ax.coastlines('110m', alpha=0.5,lw=0.5)
    #ax.coastlines(alpha=0.5,lw=0.5)

    tmp = gph[t,:,:] 
    tmp = (tmp-N.min(tmp))/(N.max(tmp)-N.min(tmp))

    levels = N.linspace(0,1,15)

    ax.contourf(lon,lat,tmp,levels=levels,cmap=P.get_cmap("terrain"),transform=ccrs.PlateCarree())
    ax.contour(lon,lat,tmp,levels=levels,colors=['white'],linewidths=2.5, transform=ccrs.PlateCarree())
    ax.contour(lon,lat,tmp,levels=levels,colors=['black'],linewidths=1, transform=ccrs.PlateCarree())

    at = AnchoredText('%s: %s'%(ftime[t],y_pred[t]),prop=dict(size=10),frameon=True,loc=3)
    at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
    at.zorder = 20    
    ax.add_artist(at)

P.tight_layout()

P.savefig('latest.png',dpi=240,bbox_inches= 'tight')

text = '''
**Plotting:**
<br/>
* GFS Weather-Types Forecast Chart
***
'''

md = Markdown(text)
console.print(md)


file = 'gfswt.csv'

csv = N.genfromtxt(file,names=True,delimiter=';',dtype=None)

gw = N.array(csv['cluster'],str)

#if(setting[1]=='GWL'): go = N.array(['W','BM','TRM','TRW','HM','SW','NW','HB','HF','WS','TB','HN','WW','TM','SE','N','HNF','S','NE'])
if(setting[1]=='GWL'): go = N.array(['W','BM','TRM','TRW','HM','SW','NW','HB','HF','TB','HN','TM','SE','N','HNF','S','NE'])
if(setting[1]=='OBJ'): go = N.array(['WT01','WT02','WT03','WT04','WT05','WT06','WT07','WT08','WT09','WT10','WT11','WT12','WT13','WT14','WT15','WT16','WT17','WT18','WT19','WT20'])

ng = len(go)
gg = N.arange(ng)

dateA = N.array(csv['dateA'],str)
doA = N.array(list(set(dateA)))
doA = N.sort(doA)

fig = P.figure(figsize=(13,6))

if(setting[0]=='EU'): P.figtext(.0,.95,'GFS 00 Forecast | European Weather-Types Sequences',fontsize=16,weight='bold',ha='left')
if(setting[0]=='JO'): P.figtext(.0,.95,'GFS 00 Forecast | Eastern Mediterranean Weather-Types Sequences',fontsize=16,weight='bold',ha='left')

P.figtext(.0,.92,'\xa9 P. Hoffmann (PIK)', fontsize=8, ha='left')

ax = P.subplot(111)

nd = 31#7
dd = N.arange(nd)

zz = N.zeros((nd,ng),float) 

xx = []
yy = []

i = 0

for dA in doA:

    id = N.where(dateA==dA)[0]    
    
    j = i 
    
    i = i+1

    x = []
    y = []

    for g in gw[id]:

        j = j+1

        ij = N.where(g==go)[0]
        
        x.append(j)
        y.append(ij)
        
        zz[j,ij] = zz[j,ij] + 1

    xx.append(x[0])
    yy.append(y[0])

P.imshow(zz.T,cmap=P.get_cmap('Reds'),interpolation="none",vmin=0,vmax=12,aspect=0.5)

#P.plot(x,y,'k-o',lw=0.5,alpha=0.3)
#P.plot(xx,yy,'k-o',lw=0.5,alpha=0.3)

for d in range(nd):
    for g in range(ng):
        
        if(zz[d,g]>0):
    
           P.text(d,g,'%i'%zz[d,g],ha='center',va='center',fontsize=10,weight='bold',zorder=11)

P.plot([i-1,i-1],[-1,ng+1],'k',lw=40,alpha=0.3)

for d in range(nd+2):

    P.plot([d-0.5,d-0.5],[-0.5,ng+0.5],color="k",zorder=15,lw=0.5)

for g in range(ng+1):

    P.plot([-0.5,nd-0.5],[g-0.5,g-0.5],color="k",zorder=15,lw=0.5)

P.yticks(gg,go,fontsize=10)
#P.xticks([1,6,11,16,21,26],['-15d','-10d','-5d',dA,'+5d','+10d'])
P.xticks([5,10,15,20,25,30],['-10d','-5d',dA,'+5d','+10d','+15d'])
P.xlim(-1,nd)
P.grid(color='k')

P.ylim(-1,ng+2)

j = -1

for i in go:

    j = j+1

    logo=image.imread("%s/%s.png"%(bundle_dir,gs[i]))
    imagebox = OffsetImage(logo, zoom=0.1)
    ab = AnnotationBbox(imagebox, (0,j), frameon = False)
    ax.add_artist(ab)

if(setting[0]=='EU'): P.text(0,ng,'CE',fontsize=12,weight='bold',color='m',ha='center',va='center',zorder=10)
if(setting[0]=='JO'): P.text(0,ng,'JO',fontsize=12,weight='bold',color='m',ha='center',va='center',zorder=10)

for d in range(nd):

    if(N.max(zz[d,:])>0):

        id = N.argsort(zz[d,:])[::-1]
        id = id[0]
        fs = 2+N.max(zz[d,:])
        
        if(fs>10): fs = 10
        
        P.text(d,ng,'%s'%(go[id]),fontsize=fs,weight='bold',ha='center',va='center',zorder=10)
        
        logo=image.imread("%s/%s.png"%(bundle_dir,gs[go[id]]))
        imagebox = OffsetImage(logo, zoom=0.1)
        ab = AnnotationBbox(imagebox, (d,ng+1), frameon = False)
        ax.add_artist(ab)

logo=image.imread("%s/Logo.png"%bundle_dir)

newax = fig.add_axes([0.01,0.01,0.05,0.1], anchor='SE')
newax.imshow(logo)
newax.axis('off')

logo=image.imread("%s/B_dt_RGB.png"%bundle_dir)

newax = fig.add_axes([0.07,0.01,0.1,0.5], anchor='SE')
newax.imshow(logo)
newax.axis('off')

P.tight_layout()

P.savefig('gfswt.png',dpi=240)

text = '''
# Program has Finished:
**generated:**
<br/>
* gfswt.png
* gfswt.csv 
* z500.png
'''

md = Markdown(text)
console.print(md)
