# GFSWT-1.0
## Very Early Warning of Critical Weather-Type Sequences
### Abstract
This program derives weather-type sequences in a categorical format from daily GFS forecast fields. This easily enable experts to overview the ensemble predicted development of synoptic weather conditions and their associated local weather phenomena. Retrospective relations between atmospheric fields and an existing catalogue of weather-type classification are used for training a decision tree and predicting weather-types from forecast fields of the past 10 and next 14 days. The predicted development of critical weather-type sequences can be easily monitored and assessed.
### Key words
**Weather Forecast, Weather-Types, Sequences, Early Warning, Extremes**
***
### Header

**script:**|_gfswt.py_
---|---
**version:**|_1.0_
**description:**|Re-identification of recurring weather-types in weather forecasts
**date:**|2024-01-25
**author:**|_Peter Hoffmann_
**email:**|_peterh@pik-potsdam.de_
**affiliation:**|_Potsdam Institute for Climate Impacts Research (PIK)_
**research department:**|_Climate Resilience_
**working group:**|_Hydro-Climatic Risks_
**project:**|_CapTainRain_
**usage:**|_python3 gfswt.py_
**compilation:**|_pyinstaller gfswt.spec_
**python_version:**|3.10.0

### Requirements
* python-dateutil==2.8.2
* siphon==0.9
* matplotlib==3.8.2
* numpy==1.23.1
* netCDF4==1.6.5
* Cartopy==0.22.0
* scipy==1.8.0
* xarray==2023.10.1
* rich==13.7.0
### Compilation
**Linux:**
```bash
> pyinstaller --clean --onefile --windowed --add-data="GWL/*:." gfswt.py
> pyinstaller gfswt.spec
```

**Windows:**
```bash
> wine ~/.wine/drive_c/Python310/Scripts/pyinstaller.exe --clean --onefile --windowed --add-data="GWL/*:." gfswt.py
> wine ~/.wine/drive_c/Python310/Scripts/pyinstaller.exe gfswt.spec
> gpg --clearsign dist/gfswt_windows/gfswt.exe
```

***
### Content

```bash
> gfswt.py # main script
> README.md
> gfswt.spec
> ./GWL/ # location of input data
```

### Method

```mermaid
graph TB

A(Z500<sub>d</sub><hr>Reanalysis) ==> C((Decision Tree<hr>Training))
B(WT<sub>k,d</sub><hr>Weather-Types) ==> C
C ==> D((Decision Tree<hr>Testing))
E(Z500<sub>t</sub><hr>Forecast Models) ==> D
D ==> F(WT<sub>k,t</sub><hr>Weather-Types)

classDef a fill:orangered,stroke:black,color:white;
class A,B a

classDef b fill:dodgerblue,stroke:black,color:white;
class E,F b

classDef c fill:black,stroke:black,color:white;
class C,D c

```

### Steps

- reading daily ERA5 reanalysis field of Z500 for Europe
- reading time series of daily weather-types
- training decision tree for the historical period
- downloading GFS forecast fields
- predicting weather-types for the forecast period
## Results

### Charts
![h:100](./gfswt.png)

**Fig.:** GFS forecast chart of the weather-types and weather-type sequences based on the past 10 day inializations. The darker the color the higher the agreement. The respective local weather phenomena for Central Europe are represented by the icons.

### Table

GFS-00-RUN|-10|-09|-08|-07|-06|-05|-04|-03|-02|-01|today|+01|+02|+03|+04|+05|+06|+07|+08|+09|+10|+11|+12|+13|+14
---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---
10d ago|W|W|W|W|TRW|TB|TB|SW|SW|TRW|BM|BM|HB|HB|NW|||||
09d ago||W|W|W|TB|TB|SW|SW|SW|TRW|TRW|TRM|TRM|W|W|W||||
08d ago|||W|W|SW|TB|SW|SW|SW|SW|TRW|TRM|W|W|W|W|W||||
07d ago||||W|TB|TB|SW|SW|SW|SW|TRW|HN|W|SW|SW|SW|TRW|TRW||||
06d ago|||||SW|TB|SW|SW|SW|SW|W|W|W|W|W|W|W|W|TRM||||
05d ago||||||TB|SW|SW|SW|SW|W|W|W|SW|W|W|SW|W|W|W||||
04d ago|||||||SW|SW|SW|SW|SW|W|W|SW|SW|SW|SW|SW|SW|SW|TRW||||
03d ago||||||||SW|SW|SW|W|W|SW|SW|SW|SW|W|W|W|W|W|BM|||||
02d ago|||||||||SW|W|W|SW|SW|SW|SW|SW|W|W|W|W|W|W|W|||||
01d ago||||||||||W|W|SW|SW|SW|SW|SW|SW|W|W|W|W|W|W|W|||||
today  |||||||||||W|SW|SW|SW|SW|SW|W|W|W|W|W|W|W|W|W||||
**SEQ**|**W**|**W**|**W**|**W**|**TB**|**TB**|**SW**|**SW**|**SW**|**SW**|**W**|**W**|**W**|**SW**|**SW**|**SW**|**W**|**W**|**W**|**W**|**W**|**W**|**W**|**W**|**W**

### Patterns
![h:100](./z500.png) 

**Fig.:** Latest GFS predicted Z500 patterns for the next 15 days.

### CSV

[gfswt.csv](./gfswt.csv)

### Demo

![](./demo.gif)