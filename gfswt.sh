#!/bin/sh

xterm -e cp gfswt_gwl_eu.joblib gfswt.joblib
xterm -e wine ./dist/gfswt_gwl_eu_2.2.exe
#xterm -e cp gfswt_obj_jo.joblib gfswt.joblib
#xterm -e wine ./dist/gfswt_obj_jo_2.2.exe
xterm -e scp gfswt.png latest.png peterh@cluster2015.pik-potsdam.de:/home/peterh/www/gfswt/
